from imports import *
with open("intents.json") as file:
    data = json.load(file)

def create_synonyms_training_set(pattern_list):
# Setting length of word limit to avoid strange pretrained word analogies
    for pattern in pattern_list:
        words = pattern.split()
        for word in words:
            if (word not in stopwords.words('english')) and len(word)>4:
                synonyms = []
                for syn in wordnet.synsets(word):
                    for l in syn.lemmas():
                        synonyms.append(l.name())
                    for syn in synonyms:
                        syn = syn.replace('_',' ')
                        test = pattern.replace(word,syn)
                        if test not in pattern_list:
                            pattern_list.append(test)
    return pattern_list

for intent in data["intents"]:
    print(intent["tag"])
    print(intent["patterns"])
    print('Finding new Patterns')
    patterns = create_synonyms_training_set(intent["patterns"])
    print(intent["tag"])
    print('NEW PATTERNS')
    print(patterns)
